package edu.uoc.android.restservice.rest.contants;

public class ApiConstants {

    // BASE URL
    public static final String BASE_GITHUB_URL = "http://159.65.168.44:3002/";

    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "materias/";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/materia/{1}";


}
